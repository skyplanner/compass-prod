/**
 * @File Name          : OpportunityTriggerUtility.cls
 * @Description        : 
 * @Author             : acantero
 * @Group              : 
 * @Last Modified By   : acantero
 * @Last Modified On   : 5/11/2020, 3:57:49 AM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    5/11/2020   acantero     Initial Version
**/
public class OpportunityTriggerUtility {
    
    public static void beforeInsert(List<Opportunity> oppList) {
        Set<ID> accIdSet = new Set<ID>();
        List<Opportunity> oppsWithAccount = new List<Opportunity>();
        for(Opportunity opp : oppList) {
            if (String.isNotBlank(opp.AccountId)) {
                oppsWithAccount.add(opp);
                accIdSet.add(opp.AccountId);
            }
        }
        if (oppsWithAccount.isEmpty()) {
            return;
        }
        //else...
        Map<ID,Account> accountList = new Map<ID,Account>([
            SELECT
                Id,
                Business_Type__c
            FROM
                Account
            WHERE
                Id in :accIdSet
        ]);
        Map<String, Schema.RecordTypeInfo> oppRecordTypeMap = 
            Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName();
        for(Opportunity oppObj : oppsWithAccount) {
            Account accountObj = accountList.get(oppObj.AccountId);
            if (accountObj <> null) {
                Schema.RecordTypeInfo recType = oppRecordTypeMap.get(accountObj.Business_Type__c);
                if (recType != null) {
                    oppObj.RecordTypeId = recType.getRecordTypeId();
                }
            }
        }
    }

}