/**
 * Test class for: EstimateIntegrator
 * @author fernando Gomez, SkyPlanner LLC
 */
@isTest
private class EstimateIntegratorTest {
	/**
	 * Tests: public void sendToQuickbooks(Id accountId)
	 */
	@isTest
	static void sendToQuickbooks_create() {
		Account acc, masterAcc;
		Contact ctc;
		QbHttpMockup mockup;
		QbCustomer customer;
		QbEstimate estimate;
		Opportunity opp;
		Integrator.IntegrationInfo info;

		// we need a master account
		masterAcc = TestHelper.getMasterAccount();
		insert masterAcc;

		// new account first
		acc = TestHelper.getSolarAccount(masterAcc.Id);
		// we mockup a synced account at this point
		// synced both happen on future methods and we
		// cannot do two stopTest in a single method
		acc.QbId__c = '001';
		acc.QbLastSyncedOn__c = System.now();
		acc.QbSyncToken__c = '0';
		insert acc;

		// we now get a new primary contact
		ctc = TestHelper.getPrimaryContact(acc.Id);

		// we need to setup a default product
		customer = new QbCustomer('001', '0', acc.Name, null, ctc.FirstName,
			null, ctc.LastName, acc.Name, '(305) 234-0090', null, null, null,
			null, null, null, null, null);
		estimate = new QbEstimate('002', '0', null,
			null, 4500, null, null, null, null, null, null);

		mockup = new QbHttpMockup(200, null,
			'{"Estimate":' + estimate.serialize() + ',' +
			'"Customer":' + customer.serialize() + '}');

		Test.setMock(HttpCalloutMock.class, mockup);

		// we can now insert thre contact
		insert ctc;

		// and we put the primary contact Id in the account
		acc.PrimaryContactId__c = ctc.Id;
		update acc;

		// now, we need an opprotunity
		opp = TestHelper.getSolarOpportunity(acc.Id);
		opp.QbProductId__c = '002';
		opp.QbProductName__c = 'Sample';
		opp.QbProductQuantity__c = 1;
		opp.QbProductUnitPrice__c = 350;
		opp.StageName = 'Engineering/Formal Proposal';
		insert opp;

		Test.startTest();
		info = new Integrator.IntegrationInfo();
		info.credentialsName = 'QbSolar';
		info.realmId = '1231234234234';
		info.recordId = opp.Id;
		EstimateIntegrator.saveToQuickbooks(
			new List<Integrator.IntegrationInfo> { info });
		Test.stopTest();

		System.assertEquals('002', [
			SELECT QbEstimateId__c
			FROM Opportunity
			WHERE Id = :opp.Id
		].QbEstimateId__c);
	}
}
