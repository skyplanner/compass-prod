/**
 * Listens to events from the Power quickbook app.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 7/7/2020
 */
@RestResource(urlMapping='/power-events')
global without sharing class PowerListener extends EventListener {
	/**
	 * Main constructor
	 */
	global PowerListener() {
		super(QuickbookAccount.getByName('QbPower'), 'Power');
		// we now proceed to process the evetns
		processEvents();
	}

	/**
	 * All events come as a POST request.
	 */
	@HttpPost
	global static void acceptPost() {
		PowerListener listener;

		try {
			listener = new PowerListener();
			RestContext.response.statusCode = 200;
		} catch (QbExceptions.BaseException ex) {
			RestContext.response.statusCode = ex.getStatusCode();
		}
	}
}
