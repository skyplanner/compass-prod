/**
 * Represents a quickbook reference.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbReference {
	public String name { get; set; }
	public String value { get; set; }

	/**
	 * @param name
	 * @param value
	 */
	public QbReference(String name, String value) {
		this.name = name;
		this.value = value;
	}
}
