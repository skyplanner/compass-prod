/**
 * Represents an phone number.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbPhone {
	public String FreeFormNumber { get; set; }

	/**
	 * @param phoneNumber
	 */
	public QbPhone(String phoneNumber) {
		FreeFormNumber = phoneNumber;
	}
}
