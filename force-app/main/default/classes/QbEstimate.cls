/**
 * Represents a quickbook estimate.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbEstimate extends QbJsonSerializable {
	public Decimal TotalAmt { get; set; }
	public QbEmail BillEmail { get; set; }

	// public XXX CustomerMemo

	public QbAddress ShipAddr { get; set; }
	public QbAddress BillAddr { get; set; }
	public QbReference CustomerRef { get; set; }
	public QbReference CustomerMemo { get; set; }

	public String TxnStatus { get; set; }
	public String EmailStatus { get; set; }

	public List<QbItemLine> Line { get; set; }
	public QbTxnTaxDetail TxnTaxDetail { get; set; }

	/**
	 * Main constructor
	 * @param Id
	 * @param SyncToken
	 * @param TxnStatus
	 * @param EmailStatus
	 * @param TotalAmt
	 * @param BillEmail
	 * @param CustomerRef
	 * @param CustomerMemo
	 * @param TxnTaxDetail
	 */
	public QbEstimate(String Id, String SyncToken, String TxnStatus, 
			String EmailStatus, Decimal TotalAmt, QbEmail BillEmail,
			QbAddress BillAddr, QbAddress ShipAddr,
			QbReference CustomerRef, QbReference CustomerMemo,
			QbTxnTaxDetail TxnTaxDetail) {
		super(Id, SyncToken);
		this.TxnStatus = TxnStatus;
		this.EmailStatus = EmailStatus;
		this.TotalAmt = TotalAmt;
		this.BillEmail = BillEmail;
		this.ShipAddr = ShipAddr;
		this.BillAddr = BillAddr;
		this.CustomerRef = CustomerRef;
		this.TxnTaxDetail = TxnTaxDetail;

		Line = new List<QbItemLine>();
	}

	/**
	 * Add a new line to the discount
	 */
	public void addLine(QbItemLine newLine) {
		if (Line == null)
			Line = new List<QbItemLine>();
		
		Line.add(newLine);
	}

	/**
	 * @param jsonStr
	 * @return a deserialized version
	 */
	public static QbEstimate deserialize(String jsonStr) {
		return (QbEstimate)JSON.deserialize(jsonStr, QbEstimate.class);
	}

	/**
	 * @param jsonStr
	 * @return a deserialized version
	 */
	public static QbEstimate deserializeMap(String jsonStr) {
		return 
			// the map brings the estimate under the Estimate
			// property, and a time property with a date span,
			// sp we have to parse to a middle object
			// and the convert the contents of Estimate 
			// which will contain the actual estimate
			((QbEstimateResponse)JSON.deserialize(
				jsonStr, QbEstimateResponse.class)).Estimate;
	}

	/**
	 * This class is just for conversion.
	 * The time stamp is not used outside.
	 */
	@TestVisible
	private class QbEstimateResponse {
		@TestVisible
		QbEstimate Estimate { get; set; }
	}
}
