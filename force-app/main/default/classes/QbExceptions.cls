/**
 * Serves as a namespace for all exceptions
 * thrown in the Quickbooks integrations.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbExceptions {
	/**
	 * Thrown when authentication fails
	 */
	public class AuthException extends BaseException {
		public override Integer getStatusCode() { 
			return 401;
		}
	}

	/**
	 * Thrown when an http fails before reaching 
	 * the Quickbooks server. E.g. Connection failure, etc.
	 */
	public class HttpException extends BaseException {
		public override Integer getStatusCode() { 
			return 500;
		}
	}

	/**
	 * Thrown when an http fails before reaching 
	 * the Quickbooks server. E.g. Connection failure, etc.
	 */
	public class BadDataException extends BaseException {
		public override Integer getStatusCode() { 
			return 400;
		}
	}

	/**
	 * Throwable base exception with a status code
	 */
	public abstract class BaseException extends Exception {
		public abstract Integer getStatusCode();
	}
}
