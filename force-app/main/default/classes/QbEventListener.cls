/**
 * Basic functionality for Event handler. These are messages
 * sent from Quicks when changes happen.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 7/7/2020
 */
public abstract class QbEventListener {
	private String verifyToken;
	private String stringPayload;
	private Blob blobPayload;
	private String intuitSignature;
	private Map<String, List<QbNotification>> notifications;

	protected String realmId;
	protected List<QbEvent> events;

	/**
	 * @param verifyToken
	 */
	public QbEventListener(String verifyToken) {
		this.verifyToken = verifyToken;
		
		blobPayload = RestContext.request.requestBody;
		stringPayload = RestContext.request.requestBody.toString();
		intuitSignature = RestContext.request.headers.get('intuit-signature');
		
		System.debug(verifyToken);
		System.debug(stringPayload);
		System.debug(intuitSignature);

		// first we authenticate.. an exception will 
		// be throw if request is no good.
		authenticate();

		// we keed debuging the payload
		System.debug(stringPayload);

		// we now extract the information in the notification
		processNotification();
	}

	/**
	 * @return the current realmId
	 */
	public String getRealmId() {
		return realmId;
	}

	/**
	 * @return all evetns received in the payload
	 */
	public List<QbEvent> getEvents() {
		return events;
	}

	/**
	 * Verifies that the request is comming from the 
	 * correct place.
	 */
	private void authenticate() {
		Blob blobToken, mac, signatureHeader64;
		String hmacKey;

		// first, we need to hash the payload using the
		// verifyToken as a key
		blobToken = Blob.valueOf(verifyToken);
		mac = Crypto.generateMac('HmacSHA256', blobPayload, blobToken);
		hmacKey = EncodingUtil.base64Encode(mac);

		// they must be identicall, otherwise this 
		// is not properly authenticated
		if (hmacKey != intuitSignature)
			throw new QbExceptions.AuthException();
	}

	/**
	 * Process the payload and generates the wrapper
	 * with the information about events.
	 */
	private void processNotification() {
		QbNotification n;

		try {
			notifications = (Map<String, List<QbNotification>>)
			JSON.deserialize(stringPayload,
				Map<String, List<QbNotification>>.class);

			n = notifications.get('eventNotifications')[0];
			realmId = n.realmId;
			events = n.dataChangeEvent.entities;
		} catch (Exception ex) { 
			throw new QbExceptions.BadDataException();
		}
	}

	/**
	 * Notification Sent from Quickbooks
	 * @author Fernando Gomez, SkyPlanner LLC
	 * @version 1.0, 7/7/2020
	 */
	private class QbNotification {
		public String realmId { get; set; }
		public DataChangeEvent dataChangeEvent { get; set; }
	}

	/**
	 * PART: Notification Sent from Quickbooks
	 * @author Fernando Gomez, SkyPlanner LLC
	 * @version 1.0, 7/7/2020
	 */
	private class DataChangeEvent {
		public List<QbEvent> entities { get; set; }
	}
}

