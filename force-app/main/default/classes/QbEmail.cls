/**
 * Represents an email address.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbEmail {
	public String Address { get; set; }

	/**
	 * @param emailAddress
	 */
	public QbEmail(String emailAddress) {
		this.Address = emailAddress;
	}
}
