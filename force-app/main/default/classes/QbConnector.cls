/**
 * Main Quickbooks dispatcher.
 * Sends and retrieves information from quickbooks.
 * Receives and retrieves formatter information already
 * converted in wrappers and ready to use. If requires 
 * the name of an already configured Named Credentials
 * instance to authenticate.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbConnector {
	private QbHttp http;
	private String realmId;

	/**
	 * @param credentialsName this is required to
	 * send authenticated request to Quickbooks.
	 * @param realmId each quickbooks company has a
	 * unique company id, they call it, the real id,
	 * this value is required to be sent in each request.
	 */
	public QbConnector(String credentialsName, String realmId) {
		http = new QbHttp(credentialsName);
		this.realmId = realmId;
	}

	/**
	 * Creates a new customer in Quickbooks.
	 * @param newCustomer
	 * @return the instance of the new customer as it was
	 * returned by quickbooks after creation.
	 */
	public QbCustomer createCustomer(QbCustomer newCustomer) {
		// we need the Id to be able to return the custom
		if (String.isNotBlank(newCustomer.Id))
			throw new QbExceptions.BadDataException(
				'The Id field of the customer cannot be specified ' +
				'if trying to create a new customer. The field must be blank.');

		return upsertCustomer(newCustomer);
	}

	/**
	 * Updates an existing customer in Quickbooks.
	 * @param customer
	 * @return the instance of the customer as it was
	 * returned by quickbooks after the update.
	 */
	public QbCustomer updateCustomer(QbCustomer customer) {
		// we need the Id to be able to return the custom
		if (String.isBlank(customer.Id) ||
				String.isBlank(customer.SyncToken))
			throw new QbExceptions.BadDataException(
				'The Id and SynToken fields of the customer cannot be blank ' +
				'when updating an existing customer.');

		// we need the latests sync token
		customer.SyncToken = getCustomer(customer.Id).SyncToken;
		return upsertCustomer(customer);
	}

	/**
	 * Upserts a customer in Quickbooks.
	 * @param customer
	 * @return the instance of the customer as it was
	 * returned by quickbooks after the update.
	 */
	public QbCustomer upsertCustomer(QbCustomer customer) {
		QbSetting setting;
		String url, returned;

		// we need to out the realId in the url
		setting = QbSetting.getByName('UpdateCustomer');
		url = QbUtility.replacePlaceholders(setting.value,
			setting.placeholderMap, getBaseValueMap());

		// we send a post with the custom initial info
		customer.sparse = false;
		returned = http.sendPost(url, customer);
		return QbCustomer.deserializeMap(returned);
	}

	/**
	 * Returns an existing customer in Quickbooks.
	 * @param newCustomer
	 * @return the instance of the existing customer as it was
	 * returned by quickbooks.
	 */
	public QbCustomer getCustomer(String customerId) {
		QbSetting setting;
		String url, returned;
		Map<String, String> valueMap;

		// we need the Id to be able to return the custom
		if (String.isBlank(customerId))
			throw new QbExceptions.BadDataException(
				'The Id field of the customer cannot be blank ' +
				'when reading an existing contact.');

		// we need to out the realmId in the url
		setting = QbSetting.getByName('ReadCustomer');
		valueMap = getBaseValueMap();

		// we also need to replace the contact Id
		// in the placeholder, so we just add to value map
		valueMap.put('customerId', customerId);

		// we buid the url based on the value map
		url = QbUtility.replacePlaceholders(setting.value,
			setting.placeholderMap, valueMap);

		// we send a post with the custom initial info
		returned = http.sendGet(url);
		return QbCustomer.deserializeMap(returned);
	}

	/**
	 * Creates a new estimate in Quickbooks.
	 * @param newCustomer
	 * @return the instance of the new estimate as it was
	 * returned by quickbooks after creation.
	 */
	public QbEstimate createEstimate(QbEstimate newEstimate) {
		// we need the Id to be able to return the custom
		if (String.isNotBlank(newEstimate.Id))
			throw new QbExceptions.BadDataException(
				'The Id field of the estimate cannot be specified ' +
				'if trying to create a new estimate. The field must be blank.');
		
		return upsertEstimate(newEstimate);
	}

	/**
	 * Updates an existing estimate in Quickbooks.
	 * @param newCustomer
	 * @return the instance of the estimate as it was
	 * returned by quickbooks.
	 */
	public QbEstimate updateEstimate(QbEstimate estimate) {
		// we need the Id to be able to return the custom
		if (String.isBlank(estimate.Id) ||
				String.isBlank(estimate.SyncToken))
			throw new QbExceptions.BadDataException(
				'The Id and SynToken fields of the estimate cannot be blank ' +
				'when updating an existing estimate.');
		
		// we need the latests sync token
		estimate.SyncToken = getEstimate(estimate.Id).SyncToken;
		return upsertEstimate(estimate);
	}

	/**
	 * Upserts an estimate in Quickbooks.
	 * @param newCustomer
	 * @return the instance of the new estimate as it was
	 * returned by quickbooks after creation.
	 */
	public QbEstimate upsertEstimate(QbEstimate estimate) {
		QbSetting setting;
		String url, returned;

		// we need to out the realId in the url
		setting = QbSetting.getByName('CreateEstimate');
		url = QbUtility.replacePlaceholders(setting.value,
			setting.placeholderMap, getBaseValueMap());

		// we send a post with the custom initial info
		estimate.sparse = false;
		returned = http.sendPost(url, estimate);
		return QbEstimate.deserializeMap(returned);
	}

	/**
	 * Returns an existing Estimate in Quickbooks.
	 * @param estimateId
	 * @return the instance of the existing Estimate as it was
	 * returned by quickbooks.
	 */
	public QbEstimate getEstimate(String estimateId) {
		QbSetting setting;
		String url, returned;
		Map<String, String> valueMap;

		// we need the Id to be able to return the custom
		if (String.isBlank(estimateId))
			throw new QbExceptions.BadDataException(
				'The Id field of the Estimate cannot be blank ' +
				'when reading an existing Estimate.');

		// we need to out the realmId in the url
		setting = QbSetting.getByName('ReadEstimate');
		valueMap = getBaseValueMap();

		// we also need to replace the contact Id
		// in the placeholder, so we just add to value map
		valueMap.put('estimateId', estimateId);

		// we buid the url based on the value map
		url = QbUtility.replacePlaceholders(setting.value,
			setting.placeholderMap, valueMap);

		// we send a post with the custom initial info
		returned = http.sendGet(url);
		return QbEstimate.deserializeMap(returned);
	}

	/**
	 * @param criteria
	 * @return QbItem
	 */
	public List<QbItem> findActiveItems(String criteria) {
		String returned;
		String likeClause;

		if (String.isNotBlank(criteria))
			likeClause = String.escapeSingleQuotes(
				criteria.trim().replaceAll('\\r?\\n', ' ').replaceAll(' ', '%'));

		// we place a query in the item object
		returned = query(
			'select * from Item ' +
			'where Type in (\'NonInventory\', \'Inventory\', \'Service\') ' +
			'and Active = true ' +
			(likeClause != null ? 'and Name like \'%' + likeClause + '%\' ' : '') +
			'maxresults 20');

		// we add the query
		return QbQueryResponse.deserialize(returned).QueryResponse.Item;
	}

	/**
	 * Returns an existing invoice in Quickbooks.
	 * @param invoiceId
	 * @return the instance of the existing invoice as it was
	 * returned by quickbooks.
	 */
	public QbInvoice getInvoice(String invoiceId) {
		QbSetting setting;
		String url, returned;
		Map<String, String> valueMap;

		// we need the Id to be able to return the custom
		if (String.isBlank(invoiceId))
			throw new QbExceptions.BadDataException(
				'The Id field of the invoice cannot be blank ' +
				'when reading an existing invoice.');

		// we need to out the realmId in the url
		setting = QbSetting.getByName('ReadInvoice');
		valueMap = getBaseValueMap();

		// we also need to replace the contact Id
		// in the placeholder, so we just add to value map
		valueMap.put('invoiceId', invoiceId);

		// we buid the url based on the value map
		url = QbUtility.replacePlaceholders(setting.value,
			setting.placeholderMap, valueMap);

		// we send a post with the custom initial info
		returned = http.sendGet(url);
		return QbInvoice.deserializeMap(returned);
	}

	/**
	 * Returns an existing payment in Quickbooks.
	 * @param newCustomer
	 * @return the instance of the existing payment as it was
	 * returned by quickbooks.
	 */
	public QbPayment getPayment(String paymentId) {
		QbSetting setting;
		String url, returned;
		Map<String, String> valueMap;

		// we need the Id to be able to return the custom
		if (String.isBlank(paymentId))
			throw new QbExceptions.BadDataException(
				'The Id field of the Payment cannot be blank ' +
				'when reading an existing Payment.');

		// we need to out the realmId in the url
		setting = QbSetting.getByName('ReadPayment');
		valueMap = getBaseValueMap();

		// we also need to replace the Payment Id
		// in the placeholder, so we just add to value map
		valueMap.put('paymentId', paymentId);

		// we buid the url based on the value map
		url = QbUtility.replacePlaceholders(setting.value,
			setting.placeholderMap, valueMap);

		// we send a post with the custom initial info
		returned = http.sendGet(url);
		return QbPayment.deserializeMap(returned);
	}

	/**
	 * @param criteria
	 * @return QbItem
	 */
	public String query(String query) {
		QbSetting setting;
		String url;
		Map<String, String> valueMap;

		// there a url for making queries
		setting = QbSetting.getByName('Query');
		valueMap = getBaseValueMap();

		// the query has to be placed in the url as well
		valueMap.put('query', query);

		url = QbUtility.replacePlaceholders(setting.value,
			setting.placeholderMap, valueMap);

		// we send a post with the custom initial info
		return http.sendGet(url);
	}

	/**
	 * @return a base map with realmId as value
	 */
	private Map<String, String> getBaseValueMap() {
		return new Map<String, String> { 'realmId' => realmId };
	}
}
