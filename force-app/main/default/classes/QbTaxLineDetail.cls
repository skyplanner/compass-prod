/**
 * Represents a quickbook QbTaxLineDetail.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbTaxLineDetail {
	public QbReference TaxRateRef { get; set; }
	public Boolean PercentBased { get; set; }
	public Decimal TaxPercent { get; set; }
	public Decimal NetAmountTaxable { get; set; }

	/**
	 * Main constructor
	 * @param TaxRateRef
	 * @param PercentBased
	 * @param TaxPercent
	 * @param NetAmountTaxable
	 */
	public QbTaxLineDetail(QbReference TaxRateRef, Boolean PercentBased,
			Decimal TaxPercent, Decimal NetAmountTaxable) {
		this.TaxRateRef = TaxRateRef;
		this.PercentBased = PercentBased;
		this.TaxPercent = TaxPercent;
		this.NetAmountTaxable = NetAmountTaxable;
	}
}
