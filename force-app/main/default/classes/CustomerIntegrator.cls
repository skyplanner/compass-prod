/**
 * Connects Compass contact with Quickbook's customer.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/24/2020
 */
public class CustomerIntegrator extends Integrator {

	/**
	 * @param credentialsName
	 * @param realmId
	 */
	public CustomerIntegrator(String credentialsName, String realmId) {
		super(credentialsName, realmId);
	}

	/**
	 * Send the specified contact to Quickbooks
	 * @param accountId
	 */
	public void sendToQuickbooks(Id accountId) {
		Account currentAccount;
		Contact primaryContact;
		QbCustomer customer;

		// we just have to feed it a record
		currentAccount = getAccount(accountId);
		primaryContact = getPrimaryContact(accountId);
		customer = createQbCustomer(currentAccount, primaryContact);

		// now we are ready to send the customer to quickboks
		// if the customer already exists, we create it,
		// if not, we update it
		if (customer.exists())
			customer = connector.updateCustomer(customer);
		else {
			customer = connector.createCustomer(customer);
			// and then we need to update the quickbook update
			currentAccount.QbId__c = customer.Id;
		}

		currentAccount.QbSyncToken__c = customer.SyncToken;
		currentAccount.QbLastSyncedOn__c = System.now();
		update currentAccount;
	}

	/**
	 * @return an instance of the QbCustomer with information
	 * from the specified contact.
	 * @param sfAccount the Account to convert to QbCustomer
	 * @param sfContact the Contact to convert to QbCustomer
	 */
	public QbCustomer createQbCustomer(Account sfAccount, Contact sfContact) {
		QbEmail email;
		QbAddress billingAddress, shippingAddress;
		QbCustomer result;
		QbPhone primaryPhone, mobile, fax, altPhone;

		email = getEmail(sfContact.Email);
		primaryPhone = getPhone(sfContact.Phone);
		mobile = getPhone(sfContact.MobilePhone);
		fax = getPhone(sfAccount.Fax);
		altPhone = getPhone(sfAccount.Phone);
		billingAddress = getBillingAddressFromAccount(sfAccount);
		shippingAddress = getShippingAddressFromAccount(sfAccount);
		
		return new QbCustomer(sfAccount.QbId__c, sfAccount.QbSyncToken__c,
			sfAccount.Name, null, sfContact.Title, 
			sfContact.FirstName, sfContact.MiddleName, sfContact.LastName,
			sfAccount.Name, primaryPhone, mobile, fax, altPhone,
			email, billingAddress, shippingAddress, sfContact.Description);
	}
	
	/**
	 * Incable method that allows sending a contact to
	 * Quickbooks from flows and process.
	 * @param accountInfos
	 */
	@InvocableMethod(label='Send Customer to Quickbooks')
	public static void sendToQuickbooks(List<IntegrationInfo> accountInfos) {
		CustomerIntegrator integrator;

		for (IntegrationInfo ci : accountInfos)
			// if we are instructed to send the request right now,
			// we go ahead..
			if (ci.sendNow == true) {
				// the integrator will take care of the communication
				integrator = new CustomerIntegrator(ci.credentialsName, ci.realmId);
				integrator.sendToQuickbooks(ci.recordId);
			}
			// if not, we transefer the interaction to a future method
			// so any callout do not interfere with DML operation...
			else 
				sendToQuickbooksAsync(ci.credentialsName, ci.realmId, ci.recordId);
	}

	/**
	 * @param credentialsName
	 * @param realmId
	 * @param accountId
	 */
	@Future(callout=true)
	public static void sendToQuickbooksAsync(
			String credentialsName, String realmId, Id accountId) {
		CustomerIntegrator integrator;
		
		// the integrator will take care of the communication
		integrator = new CustomerIntegrator(credentialsName, realmId);
		integrator.sendToQuickbooks(accountId);
	}

	/**
	 * @return all contacts with those specified sf ids
	 * and with the info needed to create a QbCustomerr
	 */
	private Account getAccount(Id accountId) {
		return [
			SELECT Id,
				QbId__c,
				QbSyncToken__c,
				Name,
				Phone,
				Fax,
				BillingState,
				BillingCity,
				BillingPostalCode,
				BillingStreet,
				BillingCountry,
				ShippingState,
				ShippingCity,
				ShippingPostalCode,
				ShippingStreet,
				ShippingCountry
			FROM Account
			WHERE Id = :accountId
		];
	}

	/**
	 * @return the primary contact of 
	 * the specified account
	 */
	private Contact getPrimaryContact(Id accountId) {
		return [
			SELECT Id,
				Name,
				Title, 
				FirstName,
				MiddleName,
				LastName,
				Email,
				Description,
				Phone,
				MobilePhone,
				OtherPhone
			FROM Contact
			WHERE AccountId = :accountId
			AND Is_Primary_Contact__c = true
			LIMIT 1
		];
	}
}
