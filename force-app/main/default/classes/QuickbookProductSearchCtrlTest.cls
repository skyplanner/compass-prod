/**
 * Test class for: QuickbookProductSearchCtrl
 * @author fernando Gomez, SkyPlanner LLC
 */
@isTest
private class QuickbookProductSearchCtrlTest {
	/**
	 * Tests: public static 
	 * List<QbItem> searchProducts(String businessType, String criteria)
	 */
	@isTest
	static void searchProducts() {
		QbQueryResponse qr;
		QbQueryResponse.QueryResult r;
		QbItem item1;
		QbHttpMockup mockup;
		List<QbItem> result;

		// this will end up being the result of the query
		item1 = new QbItem();
		item1.FullyQualifiedName = 'p1';
		item1.Name = 'p1';
		item1.Type = null;
		item1.TrackQtyOnHand = true;
		item1.IncomeAccountRef = new QbReference('zz', 'xx');
		item1.Taxable = true;
		item1.Active = true;

		qr = new QbQueryResponse();
		r = new QbQueryResponse.QueryResult();
		r.Item = new List<QbItem> { item1 };
		r.startPosition = 0;
		r.maxResults = 5;
		r.Customer = null;
		r.Estimate = null;
		r.Invoice = null;
		r.Payment = null;
		qr.QueryResponse = r;

		mockup = new QbHttpMockup(200, null, qr.serialize());
		Test.setMock(HttpCalloutMock.class, mockup);

		Test.startTest();
		result = QuickbookProductSearchCtrl.searchProducts('Solar', 'p');
		Test.stopTest();

		System.assertEquals(r.Item.size(), result.size());
	}

	/**
	 * Tests: public static 
	 * public static void saveDefaultProduct(String businessType,
	 * 		Id opportunityId, QbItem product, Integer qty)
	 */
	@isTest
	static void saveDefaultProduct() {
		Account acc, masterAcc;
		Contact ctc;
		QbHttpMockup mockup;
		Opportunity opp;
		QbItem item1;
		QbCustomer customer;

		// we need a master account
		masterAcc = TestHelper.getMasterAccount();
		insert masterAcc;

		// new account first
		acc = TestHelper.getSolarAccount(masterAcc.Id);
		// we mockup a synced account at this point
		// synced both happen on future methods and we
		// cannot do two stopTest in a single method
		acc.QbId__c = '001';
		acc.QbLastSyncedOn__c = System.now();
		acc.QbSyncToken__c = '0';
		insert acc;

		// we now get a new primary contact
		ctc = TestHelper.getPrimaryContact(acc.Id);
		insert ctc;

		// and we put the primary contact Id in the account
		acc.PrimaryContactId__c = ctc.Id;
		update acc;

		customer = new QbCustomer('001', '0', acc.Name, null, ctc.FirstName,
			null, ctc.LastName, acc.Name, '(305) 234-0090', null, null, null,
			null, null, null, null, null);
		mockup = new QbHttpMockup(200, null,
			'{"Customer":' + customer.serialize() + '}');		
		Test.setMock(HttpCalloutMock.class, mockup);
		
		// now, we need an opprotunity
		opp = TestHelper.getSolarOpportunity(acc.Id);
		insert opp;

		// this will end up being the result of the query
		item1 = new QbItem();
		item1.Id = '004';
		item1.FullyQualifiedName = 'p1';
		item1.Name = 'p1';
		item1.TrackQtyOnHand = true;
		item1.IncomeAccountRef = new QbReference('zz', 'xx');
		item1.Taxable = true;
		item1.Active = true;

		Test.startTest();
		QuickbookProductSearchCtrl.saveDefaultProduct('Solar', opp.Id, item1, 1);
		Test.stopTest();

		System.assertEquals('004', [
			SELECT QbProductId__c
			FROM Opportunity
			WHERE Id = :opp.Id
		].QbProductId__c);
	}
}
