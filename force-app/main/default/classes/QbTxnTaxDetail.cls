/**
 * Represents a quickbook TxnTaxDetail.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbTxnTaxDetail {
	public Decimal TotalTax { get; set; }
	public List<QbTaxLine> TaxLine { get; set; }
	
	/**
	 * Main constructor
	 * @param TotalTax
	 */
	public QbTxnTaxDetail(Decimal TotalTax) {
		this.TotalTax = TotalTax;
	}

	/**
	 * Add a new line to the discount
	 * @param newTaxLine
	 */
	public void addLine(QbTaxLine newTaxLine) {
		if (TaxLine == null)
			TaxLine = new List<QbTaxLine>();
		
		TaxLine.add(newTaxLine);
	}
}
