/**
 * Represents a quickbook Payment.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbPayment extends QbJsonSerializable {
	public String domain { get; set; }
	public QbReference DepositToAccountRef { get; set; }
	public Decimal UnappliedAmt { get; set; }
	public String TxnDate { get; set; }
	public Decimal TotalAmt { get; set; }
	public Boolean ProcessPayment { get; set; }
	
	public QbReference CustomerRef { get; set; }
	public List<QbItemLine> Line { get; set; }

	/**
	 * @param jsonStr
	 * @return a deserialized version
	 */
	public static QbPayment deserializeMap(String jsonStr) {
		return 
			// the map brings the Payment under the Payment
			// property, and a time property with a date span,
			// sp we have to parse to a middle object
			// and the convert the contents of Payment 
			// which will contain the actual Payment
			((QbPaymentResponse)JSON.deserialize(
				jsonStr, QbPaymentResponse.class)).Payment;
	}

	/**
	 * This class is just for conversion.
	 * The time stamp is not used outside.
	 */
	private class QbPaymentResponse {
		QbPayment Payment { get; set; }
	}
}
