/**
 * Test class for: CustomerIntegrator
 * @author fernando Gomez, SkyPlanner LLC
 */
@isTest
private class CustomerIntegratorTest {
	/**
	 * Tests: public void sendToQuickbooks(Id accountId)
	 */
	@isTest
	static void sendToQuickbooks_createSolar() {
		Account acc, acc2, masterAcc;
		Contact ctc;
		QbHttpMockup mockup;
		QbCustomer.QbCustomerResponse response;
		QbCustomer customer;
		CustomerIntegrator integrator;
		Integrator.IntegrationInfo info;

		// we need a master account
		masterAcc = TestHelper.getMasterAccount();
		insert masterAcc;

		// new account first
		acc = TestHelper.getSolarAccount(masterAcc.Id);
		// we can insert the account now, nothing should
		// happen if there is no primary contact
		insert acc;

		// we now get a new primary contact
		ctc = TestHelper.getPrimaryContact(acc.Id);

		// before inserting the contact,
		// we need to prepare a mockup response
		// the new customer that will be returned 
		// by quickbooks
		customer = new QbCustomer('001', '0', acc.Name, null, ctc.FirstName,
			null, ctc.LastName, acc.Name, '(305) 234-0090', null, null, null,
			null, null, null, null, null);
		response = new QbCustomer.QbCustomerResponse();
		response.Customer = customer;
		mockup = new QbHttpMockup(200, null, JSON.serialize(response));
		Test.setMock(HttpCalloutMock.class, mockup);

		// we insert the contact
		insert ctc;

		// and now we are ready to insert the contact...
		// this should trigger the integration
		Test.startTest();
		info = new Integrator.IntegrationInfo();
		info.credentialsName = 'QbSolar';
		info.realmId = '1231234234234';
		info.recordId = acc.Id;
		CustomerIntegrator.sendToQuickbooks(
			new List<Integrator.IntegrationInfo> { info });
		Test.stopTest();

		// lets make sure the account was updated
		acc = [
			SELECT QbId__c,
				QbLastSyncedOn__c
			FROM Account
			WHERE Id = :acc.Id
		];

		System.assertEquals('001', acc.QbId__c);
		System.assert(acc.QbLastSyncedOn__c != null);
	}

	/**
	 * Tests: public void sendToQuickbooks(Id accountId)
	 */
	@isTest
	static void sendToQuickbooks_createPower() {
		Account acc, acc2, masterAcc;
		Contact ctc;
		QbHttpMockup mockup;
		QbCustomer.QbCustomerResponse response;
		QbCustomer customer;
		CustomerIntegrator integrator;
		Integrator.IntegrationInfo info;

		// we need a master account
		masterAcc = TestHelper.getMasterAccount();
		insert masterAcc;

		// new account first
		acc = TestHelper.getPowerAccount(masterAcc.Id);
		// we can insert the account now, nothing should
		// happen if there is no primary contact
		insert acc;

		// we now get a new primary contact
		ctc = TestHelper.getPrimaryContact(acc.Id);

		// before inserting the contact,
		// we need to prepare a mockup response
		// the new customer that will be returned 
		// by quickbooks
		customer = new QbCustomer('001', '0', acc.Name, null, ctc.FirstName,
			null, ctc.LastName, acc.Name, '(305) 234-0090', null, null, null,
			null, null, null, null, null);
		response = new QbCustomer.QbCustomerResponse();
		response.Customer = customer;
		mockup = new QbHttpMockup(200, null, JSON.serialize(response));
		Test.setMock(HttpCalloutMock.class, mockup);

		// we insert the contact
		insert ctc;

		// and now we are ready to insert the contact...
		// this should trigger the integration
		Test.startTest();
		info = new Integrator.IntegrationInfo();
		info.credentialsName = 'QbPower';
		info.realmId = '1231234234234';
		info.recordId = acc.Id;
		CustomerIntegrator.sendToQuickbooks(
			new List<Integrator.IntegrationInfo> { info });
		Test.stopTest();

		// lets make sure the account was updated
		acc = [
			SELECT QbId__c,
				QbLastSyncedOn__c
			FROM Account
			WHERE Id = :acc.Id
		];

		System.assertEquals('001', acc.QbId__c);
		System.assert(acc.QbLastSyncedOn__c != null);
	}
}
