/**
 * Listens to events from the Solar quickbook app.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 7/7/2020
 */
@RestResource(urlMapping='/solar-events')
global without sharing class SolarListener extends EventListener {
	/**
	 * Main constructor
	 */
	global SolarListener() {
		super(QuickbookAccount.getByName('QbSolar'), 'Solar');
		// we now proceed to process the evetns
		processEvents();
	}

	/**
	 * All events come as a POST request.
	 */
	@HttpPost
	global static void acceptPost() {
		SolarListener listener;

		try {
			listener = new SolarListener();
			RestContext.response.statusCode = 200;
		} catch (QbExceptions.BaseException ex) {
			RestContext.response.statusCode = ex.getStatusCode();
		}
	}
}
