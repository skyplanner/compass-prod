/**
 * Represent a query response. This is what is returned
 * by Quickbook when we make a query.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/19/2020
 */
public class QbQueryResponse extends QbJsonSerializable {
	public QueryResult QueryResponse { get; set; }

	/**
	 * @param jsonStr
	 * @return a deserialized version
	 */
	public static QbQueryResponse deserialize(String jsonStr) {
		return (QbQueryResponse)JSON.deserialize(jsonStr, QbQueryResponse.class);
	}

	/**
	 * Contains resulting object,
	 * position, offset, etc...
	 */
	public class QueryResult {
		public Integer startPosition { get; set; }
		public Integer maxResults { get; set; }

		// all possible items
		public List<QbItem> Item { get; set; }
		public List<QbCustomer> Customer { get; set; }
		public List<QbEstimate> Estimate { get; set; }
		public List<QbInvoice> Invoice { get; set; }
		public List<QbPayment> Payment { get; set; }
	}
}
