/**
 * Connects Compass with Quickbook's customer.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/24/2020
 */
public abstract class Integrator {
	protected QbConnector connector;
	protected QuickbookAccount qbAccount;

	/**
	 * Accepts the credentials and reald Id already established
	 * @param credentialsName
	 * @param realmId
	 */
	public Integrator(String credentialsName, String realmId) {
		// we build the connector using the credentials name specified
		qbAccount = QuickbookAccount.getByName(credentialsName);
		connector = new QbConnector(credentialsName, realmId);
	}

	/**
	 * Accepts the business type, and deducts the credentials and realms from it
	 * @param businessType
	 */
	public Integrator(String businessType) {
		// we need to obtain the current account's settings
		switch on businessType {
			when 'Solar' {
				qbAccount = QuickbookAccount.getByName('QbSolar');
			}
			when 'Power' {
				qbAccount = QuickbookAccount.getByName('QbPower');
			}
			when 'Maritime' {
				qbAccount = QuickbookAccount.getByName('QbMaritime');
			}
			when else {
				throw new IntegratorException(
					'Incorrect Business Type. Review the ' +
					'Business Type of the related account. ' +
					'It must be either Solar, Power, or Maritime.');
			}
		}

		// we build the connector using the credentials name specified
		connector = new QbConnector(qbAccount.namedCredential, qbAccount.realmId);
	}

	/**
	 * @param email
	 */
	protected QbEmail getEmail(String email) {
		if (String.isNotBlank(email))
			return new QbEmail(email);

		return null;
	}

	/**
	 * @param phone
	 */
	protected QbPhone getPhone(String phone) {
		if (String.isNotBlank(phone))
			return new QbPhone(phone);

		return null;
	}

	/**
	 * @param acc
	 */
	protected QbAddress getBillingAddressFromAccount(Account acc) {
		QbAddress result = null;

		if (String.isNotBlank(acc.BillingState) ||
				String.isNotBlank(acc.BillingCity) ||
				String.isNotBlank(acc.BillingPostalCode) ||
				String.isNotBlank(acc.BillingStreet) ||
				String.isNotBlank(acc.BillingCountry))
			result = new QbAddress(acc.BillingState,
				acc.BillingCity, acc.BillingPostalCode,
				acc.BillingStreet, acc.BillingCountry);
		
		return result;
	}
	
	/**
	 * @param acc
	 */
	protected QbAddress getShippingAddressFromAccount(Account acc) {
		QbAddress result = null;

		if (String.isNotBlank(acc.ShippingState) ||
				String.isNotBlank(acc.ShippingCity) ||
				String.isNotBlank(acc.ShippingPostalCode) ||
				String.isNotBlank(acc.ShippingStreet) ||
				String.isNotBlank(acc.ShippingCountry))
			result = new QbAddress(acc.ShippingState,
				acc.ShippingCity, acc.ShippingPostalCode,
				acc.ShippingStreet, acc.ShippingCountry);
		
		return result;
	}

	/**
	 * Inner class to store invokable information
	 */
	public class IntegrationInfo {
		@InvocableVariable(required=true)
		public String credentialsName;
		
		@InvocableVariable(required=true)
		public String realmId;

		@InvocableVariable(required=true)
		public Id recordId;

		/**
		 * if true, the request is send right now.
		 * if false, the request is send in a future method.
		 */
		@InvocableVariable
		public Boolean sendNow;
	}

	/**
	 * Throwable exception
	 */
	public class IntegratorException extends Exception { }
}
