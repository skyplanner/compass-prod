/**
 * Represents a quickbook address.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbAddress {
	public String CountrySubDivisionCode { get; set; }
	public String City { get; set; }
	public String PostalCode { get; set; }
	public String Line1 { get; set; }
	public String Country { get; set; }

	/**
	 * @param CountrySubDivisionCode
	 * @param City
	 * @param PostalCode
	 * @param Line1
	 * @param Country
	 */
	public QbAddress(String CountrySubDivisionCode, String City,
			String PostalCode, String Line1, String Country) {
		this.CountrySubDivisionCode = CountrySubDivisionCode;
		this.City = City;
		this.PostalCode = PostalCode;
		this.Line1 = Line1;
		this.Country = Country;
	}
}
