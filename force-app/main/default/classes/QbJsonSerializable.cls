/**
 * Implementing classes can be serializable.
 * Serializable classes can be sent to quick in
 * the body of a request.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public abstract class QbJsonSerializable {
	@AuraEnabled
	public String Id { get; set; }

	@AuraEnabled
	public String SyncToken { get; set; }

	public Boolean sparse { get; set; }

	/**
	 * @param Id
	 * @param SyncToken
	 */
	public QbJsonSerializable(String Id, String SyncToken) {
		sparse = false;

		this.Id = Id;
		this.SyncToken = SyncToken;
	}

	/**
	 * Parameterless constructor for convenience.
	 */
	public QbJsonSerializable() {
		sparse = false;
	}

	/**
	 * @return true if the record already exists
	 * in quickbooks
	 */
	public Boolean exists() {
		return String.isNotBlank(Id) && String.isNotBlank(SyncToken);
	}

	/**
	 * @param latestSynToken
	 */
	public void increaseSyncToken(String latestSynToken) {
		Integer s = Integer.valueOf(latestSynToken);
		SyncToken = s + 1 + '';
	}

	/**
	 * @param sparse
	 */
	public void setSparse(Boolean sparse) {
		this.sparse = sparse;
	}
	
	/**
	 * @return a JSON string representing the current
	 * state of the implementing instance.
	 */
	public String serialize() {
		return JSON.serialize(this, true);
	}
}
