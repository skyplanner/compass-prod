/**
 * Test class for: SolarListener
 * @author fernando Gomez, SkyPlanner LLC
 */
@isTest
private class SolarListenerTest {
	/**
	 * Tests: global static void acceptPost()
	 */
	@isTest
	static void acceptPostEstimate() {
		Account acc, masterAcc;
		Contact ctc;
		Opportunity opp;
		String verifyToken, payload, intuitSignature;
		RestRequest request;
		RestResponse response;
		QuickbookAccount__mdt qba;
		QbEstimate estimate;
		QbCustomer customer;
		QbHttpMockup mockup;

		// we need a master account
		masterAcc = TestHelper.getMasterAccount();
		insert masterAcc;

		// new account first
		acc = TestHelper.getSolarAccount(masterAcc.Id);
		// we mockup a synced account at this point
		// synced both happen on future methods and we
		// cannot do two stopTest in a single method
		acc.QbId__c = '001';
		acc.QbLastSyncedOn__c = System.now();
		acc.QbSyncToken__c = '0';
		insert acc;

		// we now get a new primary contact
		ctc = TestHelper.getPrimaryContact(acc.Id);
		insert ctc;

		// and we put the primary contact Id in the account
		acc.PrimaryContactId__c = ctc.Id;
		update acc;

		// now, we need an opprotunity
		opp = TestHelper.getSolarOpportunity(acc.Id);
		opp.QbEstimateId__c = '157';
		insert opp;

		verifyToken = '8a72cfbf-7c26-4fb8-b7f0-e2abf244b2ce';
		payload = '{"eventNotifications":[{"realmId":"4620816365048867070"' +
			',"dataChangeEvent":{"entities":[{"name":"Estimate","id":"157",' +
			'"operation":"Emailed","lastUpdated":"2020-07-30T22:20:41.000Z"}]}}]}';
		intuitSignature = 'XMEPF3ET/8YPTXIwgEoGieqfCRpnnAPBvIESyz44/Ig=';

		request = new RestRequest();
		request.headers.put('intuit-signature', intuitSignature);
		request.requestBody = Blob.valueOf(payload);

		response = new RestResponse();

		RestContext.request = request;
		RestContext.response = response;
		QuickbookAccount.testVerificationToken = verifyToken;

		customer = new QbCustomer('001', '0', acc.Name, null, ctc.FirstName,
			null, ctc.LastName, acc.Name, '(305) 234-0090', null, null, null,
			null, null, null, null, null);

		estimate = new QbEstimate('157', '0', null,
			null, 4500, null, null, null, null, null, null);
		estimate.TxnStatus = 'Pending';
		estimate.EmailStatus = 'Emailed';
				
		mockup = new QbHttpMockup(200, null,
			'{"Estimate":' + estimate.serialize() + ',' +
			'"Customer":' + customer.serialize() + '}');
		Test.setMock(HttpCalloutMock.class, mockup);

		// let's call this stuff
		Test.startTest();
		SolarListener.acceptPost();
		Test.stopTest();

		System.assertEquals(1, [
			SELECT COUNT()
			FROM QuickbooksActivity__c
			WHERE Opportunity__c = :opp.Id
		]);
	}

	/**
	 * Tests: global static void acceptPost()
	 */
	@isTest
	static void acceptPostInvoice() {
		Account acc, masterAcc;
		Contact ctc;
		Opportunity opp;
		String verifyToken, payload, intuitSignature;
		RestRequest request;
		RestResponse response;
		QbCustomer customer;
		QbEstimate estimate;
		QbHttpMockup mockup;
		QbInvoice invoice;
		QbTxReference ref;

		// we need a master account
		masterAcc = TestHelper.getMasterAccount();
		insert masterAcc;

		// new account first
		acc = TestHelper.getSolarAccount(masterAcc.Id);
		// we mockup a synced account at this point
		// synced both happen on future methods and we
		// cannot do two stopTest in a single method
		acc.QbId__c = '001';
		acc.QbLastSyncedOn__c = System.now();
		acc.QbSyncToken__c = '0';
		insert acc;

		// we now get a new primary contact
		ctc = TestHelper.getPrimaryContact(acc.Id);
		insert ctc;

		// and we put the primary contact Id in the account
		acc.PrimaryContactId__c = ctc.Id;
		update acc;

		// now, we need an opprotunity
		opp = TestHelper.getSolarOpportunity(acc.Id);
		opp.QbEstimateId__c = '157';
		insert opp;

		verifyToken = '8a72cfbf-7c26-4fb8-b7f0-e2abf244b2ce';
		payload = '{"eventNotifications":[{"realmId":"4620816365048867070",' +
			'"dataChangeEvent":{"entities":[{"name":"Invoice","id":"158",' + 
			'"operation":"Create","lastUpdated":"2020-07-30T22:51:17.000Z"}]}}]}';
		intuitSignature = 'CI5kX3BmTN7LuMinwH5z2YxWNRuEz4PlZIPS5Gg9xuY=';

		request = new RestRequest();
		request.headers.put('intuit-signature', intuitSignature);
		request.requestBody = Blob.valueOf(payload);

		response = new RestResponse();

		RestContext.request = request;
		RestContext.response = response;
		QuickbookAccount.testVerificationToken = verifyToken;

		customer = new QbCustomer('001', '0', acc.Name, null, ctc.FirstName,
			null, ctc.LastName, acc.Name, '(305) 234-0090', null, null, null,
			null, null, null, null, null);
		estimate = new QbEstimate('157', '0', null,
			null, 4500, null, null, null, null, null, null);
		estimate.TxnStatus = 'Pending';
		estimate.EmailStatus = 'Emailed';

		invoice = new QbInvoice();
		invoice.Id = '158';
		invoice.EmailStatus = 'Sent';
		invoice.TotalAmt = 5600;
		invoice.TxnDate = null;
		invoice.domain = null;
		invoice.PrintStatus = null;
		invoice.EmailStatus = null;
		invoice.DueDate = null;
		invoice.ApplyTaxAfterDiscount = null;
		invoice.DocNumber = null;
		invoice.SalesTermRef = null;
		invoice.CustomerMemo = null;
		invoice.Deposit = null;
		invoice.Balance = null;
		invoice.CustomerRef = null;
		invoice.BillEmail = null;
		invoice.ShipAddr = null;
		invoice.BillAddr = null;

		ref = new QbTxReference();
		ref.TxnId = '157';
		ref.TxnType = 'Estimate';

		invoice.LinkedTxn = new List<QbTxReference> { ref };

		mockup = new QbHttpMockup(200, null, 
			'{"Estimate":' + estimate.serialize() + ',' +
			'"Invoice":' + invoice.serialize() + ',' +
			'"Customer":' + customer.serialize() + '}');

		Test.setMock(HttpCalloutMock.class, mockup);

		// let's call this stuff
		Test.startTest();
		SolarListener.acceptPost();
		Test.stopTest();

		System.assertEquals(1, [
			SELECT COUNT()
			FROM QuickbooksActivity__c
			WHERE Opportunity__c = :opp.Id
		]);
	}

	/**
	 * Tests: global static void acceptPost()
	 */
	@isTest
	static void acceptPostPayment() {
		Account acc, masterAcc;
		Contact ctc;
		Opportunity opp;
		String verifyToken, payload, intuitSignature;
		RestRequest request;
		RestResponse response;
		QuickbookAccount__mdt qba;
		QbEstimate estimate;
		QbHttpMockup mockup;
		QbInvoice invoice;
		QbTxReference ref, pref;
		QbPayment payment;
		QbItemLine itemLine;
		QbDiscountLineDetail dld;
		QbCustomer customer;

		// we need a master account
		masterAcc = TestHelper.getMasterAccount();
		insert masterAcc;

		// new account first
		acc = TestHelper.getSolarAccount(masterAcc.Id);
		// we mockup a synced account at this point
		// synced both happen on future methods and we
		// cannot do two stopTest in a single method
		acc.QbId__c = '001';
		acc.QbLastSyncedOn__c = System.now();
		acc.QbSyncToken__c = '0';
		insert acc;

		// we now get a new primary contact
		ctc = TestHelper.getPrimaryContact(acc.Id);
		insert ctc;

		// and we put the primary contact Id in the account
		acc.PrimaryContactId__c = ctc.Id;
		update acc;

		// now, we need an opprotunity
		opp = TestHelper.getSolarOpportunity(acc.Id);
		opp.QbEstimateId__c = '157';
		insert opp;

		verifyToken = '8a72cfbf-7c26-4fb8-b7f0-e2abf244b2ce';
		payload = '{"eventNotifications":[{"realmId":"4620816365048867070",' +
			'"dataChangeEvent":{"entities":[{"name":"Payment","id":"159",' +
			'"operation":"Create","lastUpdated":"2020-07-30T23:04:38.000Z"}]}}]}';
		intuitSignature = 'C+y6qUbIDW1yMdIibCmlmU4eskcgz7VwqT4dz+hFO7c=';

		request = new RestRequest();
		request.headers.put('intuit-signature', intuitSignature);
		request.requestBody = Blob.valueOf(payload);

		response = new RestResponse();

		RestContext.request = request;
		RestContext.response = response;
		QuickbookAccount.testVerificationToken = verifyToken;

		customer = new QbCustomer('001', '0', acc.Name, null, ctc.FirstName,
			null, ctc.LastName, acc.Name, '(305) 234-0090', null, null, null,
			null, null, null, null, null);

		estimate = new QbEstimate('157', '0', null,
			null, 4500, null, null, null, null, null, null);
		estimate.TxnStatus = 'Pending';
		estimate.EmailStatus = 'Emailed';

		invoice = new QbInvoice();
		invoice.Id = '158';
		invoice.EmailStatus = 'Sent';
		invoice.TotalAmt = 5600;

		ref = new QbTxReference();
		ref.TxnId = '157';
		ref.TxnType = 'Estimate';

		invoice.LinkedTxn = new List<QbTxReference> { ref };

		payment = new QbPayment();
		payment.domain = null;
		payment.DepositToAccountRef = null;
		payment.UnappliedAmt = null;
		payment.TxnDate = null;
		payment.TotalAmt = 400;
		payment.ProcessPayment = null;
		payment.CustomerRef = null;

		pref = new QbTxReference();
		pref.TxnId = '158';
		pref.TxnType = 'Invoice';

		dld = new QbDiscountLineDetail();
		dld.DiscountAccountRef = null;
		dld.PercentBased = null;
		dld.DiscountPercent = null;

		itemLine = new QbItemLine(null, 300, null, 1, null, dld);
		itemLine.LinkedTxn = new List<QbTxReference> { pref };

		payment.Line = new List<QbItemLine> { itemLine };

		mockup = new QbHttpMockup(200, null, 
			'{"Estimate":' + estimate.serialize() + ',' +
			'"Invoice":' + invoice.serialize() + ',' +
			'"Payment":' + payment.serialize() + ',' +
			'"Customer":' + customer.serialize() + '}');

		Test.setMock(HttpCalloutMock.class, mockup);

		// let's call this stuff
		Test.startTest();
		SolarListener.acceptPost();
		Test.stopTest();

		System.assertEquals(1, [
			SELECT COUNT()
			FROM QuickbooksActivity__c
			WHERE Opportunity__c = :opp.Id
		]);
	}
}
