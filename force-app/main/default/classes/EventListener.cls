/**
 * Basic functionality for Compass' QUickbooks Event handler. 
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 7/7/2020
 */
public abstract class EventListener extends QbEventListener {
	private String businessType;
	private QuickbookAccount qAccount;

	/**
	 * @param qAccount
	 * @param businessType
	 */
	public EventListener(QuickbookAccount qAccount, String businessType) {
		super(qAccount.verificationToken);
		this.qAccount = qAccount;
		this.businessType = businessType;
	}

	/**
	 * Proces the received  events from QUickbooks.
	 * In this case, we create record of activities 
	 * in the oppotunity
	 */
	protected void processEvents() {
		// depending on the operation, we need to extract
		// the estimate Id...
		// THE FORMAT SENT FROM QUICKBOOKS IS SHAPED
		// TO SEND A LIST OF EVENTS, BUT ONLY ONE EVENT IS SENT 
		// AT A TIME.
		for (QbEvent e : getEvents()) {
			switch on e.name {
				when 'Customer' {
					processCustomer(e);
				}
				when 'Estimate' {
					processEstimate(e);
				}
				when 'Invoice' {
					processInvoice(e);
				}
				when 'Payment' {
					processPayment(e);
				}
				when else {
					// we do nothing here at this point...
				}
			}
		}
	}

	/**
	 * Saves a customer notification (activity) 
	 * to the related account, if any.
	 * @param e
	 */
	private void processCustomer(QbEvent e) {
		QbConnector connector;
		Account acc;
		QbCustomer customer;
		String billingStreet, billingCity, billingState,
			billingPostalCode, billingCountry,
			shippingStreet, shippingCity, shippingState,
			shippingPostalCode, shippingCountry;

		// we search for an account with the id
		acc = getAccount(e.id);

		// if we find one to relate the activity to,
		// we create the record
		if (acc != null) {
			connector = new QbConnector(qAccount.namedCredential, qAccount.realmId);
			customer = connector.getCustomer(e.id);

			if (customer.BillAddr != null) {
				billingStreet = customer.BillAddr.Line1;
				billingCity = customer.BillAddr.City;
				billingState = customer.BillAddr.CountrySubDivisionCode;
				billingPostalCode = customer.BillAddr.PostalCode;
				billingCountry = customer.BillAddr.Country;
			}

			if (customer.ShipAddr != null) {
				shippingStreet = customer.ShipAddr.Line1;
				shippingCity = customer.ShipAddr.City;
				shippingState = customer.ShipAddr.CountrySubDivisionCode;
				shippingPostalCode = customer.ShipAddr.PostalCode;
				shippingCountry = customer.ShipAddr.Country;
			}

			insert new QuickbooksCustomerActivity__c(
				Account__c = acc.Id,
				PrimaryContact__c = acc.PrimaryContactId__c,
				Operation__c = e.operation,
				QbId__c = e.id,
				FullyQualifiedName__c = customer.FullyQualifiedName,
				GivenName__c = customer.givenName,
				MiddleName__c = customer.MiddleName,
				FamilyName__c = customer.FamilyName,
				Title__c = customer.Title,
				Email__c = customer.PrimaryEmailAddr != null ?
					customer.PrimaryEmailAddr.Address : null,
				Phone__c = customer.PrimaryPhone != null ?
					customer.PrimaryPhone.FreeFormNumber : null,
				Mobile__c = customer.Mobile != null ?
					customer.Mobile.FreeFormNumber : null,
				Fax__c = customer.Fax != null ?
					customer.Fax.FreeFormNumber : null,
				AltPhone__c = customer.AlternatePhone != null ?
					customer.AlternatePhone.FreeFormNumber : null,
				Notes__c = customer.Notes,
				BillingStreet__c = billingStreet,
				BillingCity__c = billingCity,
				BillingState__c = billingState,
				BillingPostalCode__c = billingPostalCode,
				BillingCountry__c = billingCountry,
				ShippingStreet__c = shippingStreet,
				ShippingCity__c = shippingCity,
				ShippingState__c = shippingState,
				ShippingPostalCode__c = shippingPostalCode,
				ShippingCountry__c = shippingCountry
			);
		}
	}

	/**
	 * Saves an estimate notification (activity) 
	 * to the related opportunity, if any.
	 * @param e
	 */
	private void processEstimate(QbEvent e) {
		QbEstimate estimate;
		QbConnector connector;
		Opportunity opp;

		// the opportunity contains the id
		// of the estimate created
		opp = getOpportunity(e.id);

		// if an opportunity was found, then we need to
		// create an activity records
		if (opp != null) {
			connector = new QbConnector(qAccount.namedCredential, qAccount.realmId);
			estimate = connector.getEstimate(e.id);
			insert new QuickbooksActivity__c(
				Operation__c = e.operation,
				QbId__c = e.id,
				QbName__c = e.name,
				QbStatus__c = estimate.TxnStatus,
				QbEmailStatus__c = estimate.EmailStatus,
				QbAmount__c = estimate.TotalAmt,
				Opportunity__c = opp.Id
			);
		}
	}

	/**
	 * Saves an invoice notification (activity) 
	 * to the related opportunity, if any.
	 * @param e
	 */
	private void processInvoice(QbEvent e) {
		QbEstimate estimate;
		QbInvoice invoice;
		QbConnector connector;
		Opportunity opp;
		
		// we need more info about this invoice...
		connector = new QbConnector(qAccount.namedCredential, qAccount.realmId);
		invoice = connector.getInvoice(e.id);

		// the estimate id is in the LinkedTxn reference
		for (QbTxReference r : invoice.LinkedTxn)
			if (r.TxnType == 'Estimate') {
				opp = getOpportunity(r.TxnId);

				if (opp != null)
					insert new QuickbooksActivity__c(
						Operation__c = e.operation,
						QbId__c = e.id,
						QbName__c = e.name,
						QbStatus__c = null,
						QbEmailStatus__c = invoice.EmailStatus,
						QbAmount__c = invoice.TotalAmt,
						Opportunity__c = opp.Id
					);
					
				break;
			}
	}

	/**
	 * Saves an payment notification (activity) 
	 * to the related opportunity, if any.
	 * @param e
	 */
	private void processPayment(QbEvent e) {
		QbEstimate estimate;
		QbInvoice invoice;
		QbPayment payment;
		QbConnector connector;
		Opportunity opp;

		// to find the estimate id,
		// we need the payment from QB
		connector = new QbConnector(qAccount.namedCredential, qAccount.realmId);
		payment = connector.getPayment(e.id);

		// the payment has a link to the invoice
		// in the TxnId field, which is inside one of the lines.
		// we try to find the link to the invoice
		if (payment.Line != null && !payment.Line.isEmpty())
			for (QbTxReference r : payment.Line[0].LinkedTxn)
				if (r.TxnType == 'Invoice') {
					// we have the invoice id, we need more
					// info about this invoice to get the estimate id
					invoice = connector.getInvoice(r.TxnId);

					// the estimate id is in the LinkedTxn reference
					for (QbTxReference r1 : invoice.LinkedTxn)
						if (r1.TxnType == 'Estimate') {
							opp = getOpportunity(r1.TxnId);

							if (opp != null)
								insert new QuickbooksActivity__c(
									Operation__c = e.operation,
									QbId__c = e.id,
									QbName__c = e.name,
									QbStatus__c = null,
									QbEmailStatus__c = null,
									QbAmount__c = payment.TotalAmt,
									Opportunity__c = opp.Id
								);

							break;
						}

					break;
				}
	}

	/**
	 * @param customerId
	 * @return the current account which needs to contain
	 * the proper business type, and the estimate Id
	 */
	private Account getAccount(String customerId) {
		try {
			return [
				SELECT Id, PrimaryContactId__c
				FROM Account
				WHERE Business_Type__c IN :qAccount.listenerSharedtypes
				AND QbId__c = :customerId
				LIMIT 1
			];
		} catch (QueryException ex) {
			return null;
		}
	}

	/**
	 * @param estimateId
	 * @return the current opportunity which needs to contain
	 * the proper business type, and the estimate Id
	 */
	private Opportunity getOpportunity(String estimateId) {
		try {
			return [
				SELECT Id
				FROM Opportunity
				WHERE RecordType.DeveloperName IN :qAccount.listenerSharedtypes
				AND QbEstimateId__c = :estimateId
				LIMIT 1
			];
		} catch (QueryException ex) {
			return null;
		}
	}
}
