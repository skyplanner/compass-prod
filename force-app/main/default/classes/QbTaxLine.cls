/**
 * Represents a quickbook QbTaxLine.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbTaxLine {
	public String DetailType { get; set; }
	public Decimal Amount { get; set; }
	public QbTaxLineDetail TaxLineDetail { get; set; }

	/**
	 * Main constructor
	 * @param Amount
	 * @param TaxLineDetail
	 */
	public QbTaxLine(Decimal Amount, QbTaxLineDetail TaxLineDetail) {
		this.Amount = Amount;
		this.TaxLineDetail = TaxLineDetail;
		DetailType = 'TaxLineDetail';
	}
}
