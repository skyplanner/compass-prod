/**
 * Represents a quickbook customer.
 * @author Fernando Gomez, SkyPlanner LLC
 * @version 1.0, 6/18/2020
 */
public class QbCustomer extends QbJsonSerializable {
	public String FullyQualifiedName { get; set; }
	public String DisplayName { get; set; }
	public String Suffix { get; set; }
	public String Title { get; set; }
	public String GivenName { get; set; }
	public String MiddleName { get; set; }
	public String FamilyName { get; set; }
	public String CompanyName { get; set; }
	
	public QbEmail PrimaryEmailAddr { get; set; }

	public QbPhone PrimaryPhone { get; set; }
	public QbPhone Mobile { get; set; }
	public QbPhone Fax { get; set; }
	public QbPhone AlternatePhone { get; set; }

	public QbAddress BillAddr { get; set; }
	public QbAddress ShipAddr { get; set; }

	public String Notes { get; set; }

	/**
	 * @param Id
	 * @param SyncToken
	 * @param FullyQualifiedName
	 * @param DisplayName
	 * @param Suffix
	 * @param Title
	 * @param GivenName
	 * @param MiddleName
	 * @param FamilyName
	 * @param CompanyName
	 * @param PrimaryPhone
	 * @param Mobile
	 * @param Fax
	 * @param AlternatePhone
	 * @param PrimaryEmailAddr
	 * @param BillAddr
	 * @param ShipAddr
	 * @param Notes
	 */
	public QbCustomer(String Id, String SyncToken, 
			String DisplayName, String Suffix, String Title, String GivenName,
			String MiddleName, String FamilyName, String CompanyName,
			QbPhone PrimaryPhone, QbPhone Mobile, QbPhone Fax, QbPhone AlternatePhone,
			QbEmail PrimaryEmailAddr, QbAddress BillAddr, QbAddress ShipAddr,
			String Notes) {
		super(Id, SyncToken);

		this.DisplayName = DisplayName;
		this.Suffix = Suffix;
		this.Title = Title;
		this.GivenName = GivenName;
		this.MiddleName = MiddleName;
		this.FamilyName = FamilyName;
		this.CompanyName = CompanyName;
		this.PrimaryPhone = PrimaryPhone;
		this.Mobile = Mobile;
		this.Fax = Fax;
		this.AlternatePhone = AlternatePhone;
		this.PrimaryEmailAddr = PrimaryEmailAddr;
		this.BillAddr = BillAddr;
		this.ShipAddr = ShipAddr;
		this.Notes = Notes;
	}
	
	/**
	 * @param jsonStr
	 * @return a deserialized version
	 */
	public static QbCustomer deserialize(String jsonStr) {
		return (QbCustomer)JSON.deserialize(jsonStr, QbCustomer.class);
	}

	/**
	 * @param jsonStr
	 * @return a deserialized version
	 */
	public static QbCustomer deserializeMap(String jsonStr) {
		return 
			// the map brings the Customer under the Customer
			// property, and a time property with a date span,
			// sp we have to parse to a middle object
			// and the convert the contents of Customer 
			// which will contain the actual Customer
			((QbCustomerResponse)JSON.deserialize(
				jsonStr, QbCustomerResponse.class)).Customer;
	}

	/**
	 * This class is just for conversion.
	 * The time stamp is not used outside.
	 */
	@TestVisible
	private class QbCustomerResponse {
		@TestVisible
		QbCustomer Customer { get; set; }
	}
}
